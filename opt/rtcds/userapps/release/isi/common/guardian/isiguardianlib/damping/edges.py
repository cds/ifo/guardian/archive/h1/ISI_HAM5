edges = [
        # state A       -->     state B 
        ('ENGAGE_DAMPING_LOOPS', 'RAMP_DAMPING_FILTERS_UP'),
        ('RAMP_DAMPING_FILTERS_UP', 'DAMPED'),
        ('DAMPED', 'RAMP_DAMPING_FILTERS_DOWN'),
        ('RAMP_DAMPING_FILTERS_DOWN', 'DISENGAGE_DAMPING_LOOPS'),
        ]
